## 说明

```bash
# 仓库拷贝
git clone https://git.code.tencent.com/club100/ui

# 切换到目录
cd club100-ui

#安装依赖
npm install

# 淘宝 cnpm
npm install --registry=https://registry.npm.taobao.org

# 测试启动
npm run dev
```

```bash
# 打包到测试
npm run build:stage

# 打包到生产
npm run build:prod
```