import request from '@/utils/request'

export function getToken() {
  return request({
    url: '/qiniu/upload/token',
    method: 'get'
  })
}

export function upload(data) {
  return request({
    url: 'https://upload-z2.qiniup.com', //华南区域上传专用链接
    method: 'post',
    data: data
  })
}

export function uploadFile(data) {
  return request({
    url: 'https://upload-z2.qiniup.com', //华南区域上传专用链接
    method: 'post',
    headers: {'Content-Type': 'multipart/form-data'},
    data: data
  })
}
