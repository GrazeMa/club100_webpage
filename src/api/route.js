import request from '@/utils/request'

/** 查询路线内容 */
export function page(query) {
  return request({
    url: '/v1/route',
    method: 'get',
    params: query
  })
}

/** 新增一条路线 */
export function insertOneRoute(data) {
  return request({
    url: '/v1/route/insertOneRoute',
    method: 'post',
    data: data
  })
}

/** 新增一条路线详情 */
export function insertOneRouteDetail(data) {
  return request({
    url: '/v1/routeDetail/insertOneRouteDetail',
    method: 'post',
    data: data
  })
}

export function updateRoute(data) {
  return request({
    url: '/v1/route/updateRoute',
    method: 'post',
    data: data
  })
}

export function updateRouteDetail(data) {
  return request({
    url: '/v1/routeDetail/updateRouteDetail',
    method: 'post',
    data: data
  })
}

/** 删除路线 */
export function doDeleteRoute(id) {
  return request({
    url: '/v1/route/' + id,
    method: 'delete'
  })
}

/** 获取城市下拉框选项 */
export function selectCityDropdownList() {
  return request({
    url: '/v1/city/selectCityDropdownList',
    method: 'get'
  })
}

/** 单个路线上线按钮操作 */
export function uploadRoute(routeId) {
  return request({
    url: '/v1/route/uploadRoute',
    method: 'post',
    data: routeId
  })
}
