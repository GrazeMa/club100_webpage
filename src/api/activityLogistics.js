import request from '@/utils/request'

/** 查询活动后勤保障列表 */
export function listActivityLogistics(query) {
  return request({
    url: '/v1/activitylogistics/listActivityLogistics',
    method: 'get',
    params: query
  })
}

/** 删除活动后勤保障信息 */
export function deleteActivityLogistics(returnActivityLogisticsInfoIds) {
  return request({
    url: '/v1/activitylogistics/' + returnActivityLogisticsInfoIds,
    method: 'delete'
  })
}

/** 查询活动后勤保障信息 */
export function getActivityLogisticsById(returnActivityLogisticsInfoId) {
  return request({
    url: '/v1/activitylogistics/getActivityLogisticsById',
    method: 'post',
    data: returnActivityLogisticsInfoId
  })
}

/** 修改活动后勤保障信息 */
export function updateActivityLogistics(data) {
  return request({
    url: '/v1/activitylogistics/updateActivityLogistics',
    method: 'post',
    data: data
  })
}

/** 添加活动后勤保障信息 */
export function addActivityLogistics(data) {
  return request({
    url: '/v1/activitylogistics/addActivityLogistics',
    method: 'post',
    data
  })
}

/** 获取活动单选下拉框选项 */
export function selectActivityDropdownList() {
  return request({
    url: '/v1/activity/selectActivityDropdownList',
    method: 'get'
  })
}

/** 获取后勤保障多选下拉框选项 */
export function selectLogisticsDropdownList() {
  return request({
    url: '/v1/logistics/selectLogisticsDropdownList',
    method: 'get'
  })
}