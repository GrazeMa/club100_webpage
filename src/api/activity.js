import request from '@/utils/request'

/** 查询活动列表 */
export function listActivities(query) {
  return request({
    url: '/v1/activity/listActivities',
    method: 'get',
    params: query
  })
}

/** 删除活动信息 */
export function deleteActivities(activityIds) {
  return request({
    url: '/v1/activity/' + activityIds,
    method: 'delete'
  })
}

/** 查询活动信息 */
export function getActivityById(activityId) {
  return request({
    url: '/v1/activity/getActivityById',
    method: 'post',
    data: activityId
  })
}

/** 审核成功操作 */
export function activityAuditSuccess(activityId) {
  return request({
    url: '/v1/activity/activityAuditSuccess',
    method: 'post',
    data: activityId
  })
}

/** 审核失败操作 */
export function activityAuditFailure(data) {
  return request({
    url: '/v1/activity/activityAuditFailure',
    method: 'post',
    data: data
  })
}

/** 修改活动信息 */
export function updateActivity(data) {
  return request({
    url: '/v1/activity/updateActivity',
    method: 'post',
    data: data
  })
}

/** 添加一条活动信息 */
export function addActivity(data) {
  return request({
    url: '/v1/activity/addActivity',
    method: 'post',
    data
  })
}

/** 保存活动信息为草稿 */
export function addActivityDraft(data) {
  return request({
    url: '/v1/activity/addActivityDraft',
    method: 'post',
    data
  })
}

/** 获取活动单选下拉框选项 */
export function selectLeaderDropdownList() {
  return request({
    url: '/v1/leader/selectLeaderDropdownList',
    method: 'get'
  })
}

/** 获取车店信息单选下拉框选项 */
export function selectBikeShopDropdownList() {
  return request({
    url: '/v1/bikeshop/selectBikeShopDropdownList',
    method: 'get'
  })
}

/** 获取后勤保障多选下拉框选项 */
export function selectLogisticsDropdownList() {
  return request({
    url: '/v1/logistics/selectLogisticsDropdownList',
    method: 'get'
  })
}

/** 获取城市下拉框选项 */
export function selectCityDropdownList() {
  return request({
    url: '/v1/city/selectCityDropdownList',
    method: 'get'
  })
}

/** 获取城市下拉框选项（包括全国选项） */
export function selectCityNationwideDropdownList() {
  return request({
    url: '/v1/city/selectCityNationwideDropdownList',
    method: 'get'
  })
}

// 导出岗位
// export function exportPost(query) {
//   return request({
//     url: '/system/post/export',
//     method: 'get',
//     params: query
//   })
// }