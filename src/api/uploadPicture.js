import request from '@/utils/request'

/** 查询俱乐部列表 */
export function listClubs(query) {
  return request({
    url: '/v1/bikeshop/list',
    method: 'get',
    params: query
  })
}

/** 查询俱乐部信息 */
export function uploadPicture(formData) {
  return request({
    url: '/v1/pizza/uploadPicture',
    method: 'post',
    data: formData
  })
}
