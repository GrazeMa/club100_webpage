import request from '@/utils/request'

/** 将 GPX 文件转为 JSON。 */
export function transformGpxFileToJson(query) {
  return request({
    url: '/v1/transform/transformGpxFileToJson',
    method: 'get',
    params: query
  })
}
