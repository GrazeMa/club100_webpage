import request from '@/utils/request'

/** 查询后勤保障列表 */
export function listLogistics(query) {
  return request({
    url: '/v1/logistics/list',
    method: 'get',
    params: query
  })
}

/** 查询后勤保障信息 */
export function getLogisticsById(logisticsId) {
  return request({
    url: '/v1/logistics/getLogisticsById',
    method: 'post',
    data: logisticsId
  })
}

/** 修改后勤保障信息 */
export function updateLogistics(data) {
  return request({
    url: '/v1/logistics/updateLogistics',
    method: 'post',
    data: data
  })
}

/** 删除后勤保障信息 */
export function deleteLogistics(logisticsIds) {
  return request({
    url: '/v1/logistics/' + logisticsIds,
    method: 'delete'
  })
}

/** 添加一条后勤保障信息 */
export function addLogistics(data) {
  return request({
    url: '/v1/logistics/addLogistics',
    method: 'post',
    data
  })
}
