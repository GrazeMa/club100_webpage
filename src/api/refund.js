import request from '@/utils/request'

/** 查询退款政策列表 */
export function listRefundPolicy(query) {
  return request({
    url: '/v1/refundpolicy/listRefundPolicy',
    method: 'get',
    params: query
  })
}

/** 删除退款政策信息 */
export function deleteRefundPolicy(returnRefundPolicyInfoIds) {
  return request({
    url: '/v1/refundpolicy/' + returnRefundPolicyInfoIds,
    method: 'delete'
  })
}

/** 查询退款政策信息 */
export function getRefundPolicyById(returnRefundPolicyInfoId) {
  return request({
    url: '/v1/refundpolicy/getRefundPolicyById',
    method: 'post',
    data: returnRefundPolicyInfoId
  })
}

/** 修改退款政策信息 */
export function updateRefundPolicy(data) {
  return request({
    url: '/v1/refundpolicy/updateRefundPolicy',
    method: 'post',
    data: data
  })
}

/** 添加退款政策信息 */
export function addRefundPolicy(data) {
  return request({
    url: '/v1/refundpolicy/addRefundPolicy',
    method: 'post',
    data
  })
}

/** 获取活动单选下拉框选项 */
export function selectActivityDropdownList() {
  return request({
    url: '/v1/activity/selectActivityDropdownList',
    method: 'get'
  })
}