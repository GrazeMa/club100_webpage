import request from '@/utils/request'

/** 查询俱乐部列表 */
export function listClubs(query) {
  return request({
    url: '/v1/bikeshop/list',
    method: 'get',
    params: query
  })
}

/** 查询俱乐部信息 */
export function getClubById(bikeShopId) {
  return request({
    url: '/v1/bikeshop/getBikeShopById',
    method: 'post',
    data: bikeShopId
  })
}

/** 修改俱乐部信息 */
export function updateClub(data) {
  return request({
    url: '/v1/bikeshop/updateBikeShop',
    method: 'post',
    data: data
  })
}

/** 删除俱乐部信息 */
export function deleteClubs(bikeShopIds) {
  return request({
    url: '/v1/bikeshop/' + bikeShopIds,
    method: 'delete'
  })
}

/** 添加一条俱乐部信息 */
export function addClub(data) {
  return request({
    url: '/v1/bikeshop/addBikeShop',
    method: 'post',
    data
  })
}

/** 获取城市下拉框选项 */
export function selectCityDropdownList() {
  return request({
    url: '/v1/city/selectCityDropdownList',
    method: 'get'
  })
}

// 导出岗位
// export function exportPost(query) {
//   return request({
//     url: '/system/post/export',
//     method: 'get',
//     params: query
//   })
// }