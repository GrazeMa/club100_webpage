import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/vue-element-admin/article/list',
    method: 'get',
    params: query
  })
}

export function fetchArticle(id) {
  return request({
    url: '/v1/atricle',
    method: 'get',
    params: {
      id
    }
  })
}

export function selectArticleById(id) {
  return request({
    url: '/v1/atricle/selectArticleById',
    method: 'get',
    params: {
      id
    }
  })
}

/** 发布内容 */
export function submit(data) {
  return request({
    url: '/v1/atricle',
    method: 'post',
    data: data
  })
}

/** 发布内容 */
// export function submit2(data) {
//   return request({
//     url: '/v1/atricle',
//     method: 'post',
//     data: data
//   })
// }

export function submit2(data) {
  console.log(data);
  return request({
    url: '/v1/route/insertOneRoute',
    method: 'post',
    data: data
  })
}

export function updateArticle(data) {
  return request({
    url: '/v1/atricle/updateArticle',
    method: 'post',
    data: data
  })
}

/** 查询内容 */
export function page(query) {
  return request({
    url: '/v1/atricle',
    method: 'get',
    params: query
  })
}

export function fetchPv(pv) {
  return request({
    url: '/vue-element-admin/article/pv',
    method: 'get',
    params: {
      pv
    }
  })
}

export function createArticle(data) {
  return request({
    url: '/vue-element-admin/article/create',
    method: 'post',
    data
  })
}

/**  */
export function doDeleteArticle(id) {
  return request({
    url: '/v1/atricle/' + id,
    method: 'delete'
  })
}
