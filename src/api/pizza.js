import request from '@/utils/request'

/** 查询披萨审核图片列表 */
export function listPizza(query) {
  return request({
    url: '/v1/pizza/listPizza',
    method: 'get',
    params: query
  })
}

/** 审核通过操作 */
export function auditPizzaSuccess(pizzaGetterId) {
  return request({
    url: '/v1/pizza/auditPizzaSuccess',
    method: 'post',
    data: pizzaGetterId
  })
}

/** 审核失败操作 */
export function auditPizzaFail(pizzaGetterId) {
  return request({
    url: '/v1/pizza/auditPizzaFail',
    method: 'post',
    data: pizzaGetterId
  })
}

/** 添加活动图片信息 */
export function addActivityPicture(data) {
  return request({
    url: '/v1/activitypicture/addActivityPicture',
    method: 'post',
    data
  })
}
