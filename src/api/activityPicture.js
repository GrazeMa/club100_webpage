import request from '@/utils/request'

/** 查询活动图片列表 */
export function listActivityPicture(query) {
  return request({
    url: '/v1/activitypicture/listActivityPicture',
    method: 'get',
    params: query
  })
}

/** 删除活动图片信息 */
export function deleteActivityPicture(returnActivityPictureInfoIds) {
  return request({
    url: '/v1/activitypicture/' + returnActivityPictureInfoIds,
    method: 'delete'
  })
}

/** 查询活动后勤保障信息 */
// export function getActivityLogisticsById(returnActivityLogisticsInfoId) {
//   return request({
//     url: '/v1/activitylogistics/getActivityLogisticsById',
//     method: 'post',
//     data: returnActivityLogisticsInfoId
//   })
// }

/** 修改活动后勤保障信息 */
// export function updateActivityLogistics(data) {
//   return request({
//     url: '/v1/activitylogistics/updateActivityLogistics',
//     method: 'post',
//     data: data
//   })
// }

/** 添加活动图片信息 */
export function addActivityPicture(data) {
  return request({
    url: '/v1/activitypicture/addActivityPicture',
    method: 'post',
    data
  })
}

/** 获取活动单选下拉框选项 */
export function selectActivityDropdownList() {
  return request({
    url: '/v1/activity/selectActivityDropdownList',
    method: 'get'
  })
}