import request from '@/utils/request'

/** 查询俱乐部列表 */
export function listOrders(query) {
  return request({
    url: '/v1/order/listOrders',
    method: 'get',
    params: query
  })
}

/** 查询俱乐部信息 */
export function getClubById(bikeShopId) {
  return request({
    url: '/v1/bikeshop/getBikeShopById',
    method: 'post',
    data: bikeShopId
  })
}

/** 修改俱乐部信息 */
export function updateClub(data) {
  return request({
    url: '/v1/bikeshop/updateBikeShop',
    method: 'post',
    data: data
  })
}

/** 删除俱乐部信息 */
export function deleteClubs(bikeShopIds) {
  return request({
    url: '/v1/bikeshop/' + bikeShopIds,
    method: 'delete'
  })
}

/** 添加一条俱乐部信息 */
export function addClub(data) {
  return request({
    url: '/v1/bikeshop/addBikeShop',
    method: 'post',
    data
  })
}

/** 获取活动单选下拉框选项 */
export function selectActivityDropdownList() {
  return request({
    url: '/v1/activity/selectActivityDropdownList',
    method: 'get'
  })
}

// 导出订单。
export function exportOrders(query) {
  return request({
    url: '/v1/order/exportOrders',
    method: 'get',
    params: query
  })
}

// 导出岗位
// export function exportPost(query) {
//   return request({
//     url: '/system/post/export',
//     method: 'get',
//     params: query
//   })
// }