import request from '@/utils/request'

/** 查询活动路线列表 */
export function listActivityRoute(query) {
  return request({
    url: '/v1/activityroute/listActivityRoute',
    method: 'get',
    params: query
  })
}

/** 删除活动路线信息 */
export function deleteActivityRoute(activityRouteIds) {
  return request({
    url: '/v1/activityroute/' + activityRouteIds,
    method: 'delete'
  })
}

/** 查询活动路线信息 */
export function getActivityRouteById(activityRouteId) {
  return request({
    url: '/v1/activityroute/getActivityRouteById',
    method: 'post',
    data: activityRouteId
  })
}

/** 修改活动路线信息 */
export function updateActivityRoute(data) {
  return request({
    url: '/v1/activityroute/updateActivityRoute',
    method: 'post',
    data: data
  })
}

/** 添加活动路线信息 */
export function addActivityRoute(data) {
  return request({
    url: '/v1/activityroute/addActivityRoute',
    method: 'post',
    data
  })
}

/** 获取活动单选下拉框选项 */
export function selectActivityDropdownList() {
  return request({
    url: '/v1/activity/selectActivityDropdownList',
    method: 'get'
  })
}