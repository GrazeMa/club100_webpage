import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/vue-element-admin/article/list',
    method: 'get',
    params: query
  })
}

export function fetchArticle(id) {
  return request({
    url: '/vue-element-admin/article/detail',
    method: 'get',
    params: {
      id
    }
  })
}

/** 发布内容 */
export function submit(data) {
  return request({
    url: '/v1/atricle',
    method: 'post',
    data: data
  })
}

/** 查询内容 */
export function page(query) {
  return request({
    url: '/v1/card',
    method: 'get',
    params: query
  })
}

export function fetchPv(pv) {
  return request({
    url: '/vue-element-admin/article/pv',
    method: 'get',
    params: {
      pv
    }
  })
}

export function createArticle(data) {
  return request({
    url: '/vue-element-admin/article/create',
    method: 'post',
    data
  })
}

export function updateCard(data) {
  return request({
    url: '/v1/card/updateCard',
    method: 'post',
    data: data
  })
}

export function doDeleteCard(id) {
  return request({
    url: '/v1/card/' + id,
    method: 'delete'
  })
}

export function addCard(data) {
  return request({
    url: '/v1/card/addCard',
    method: 'post',
    data
  })
}

/** 获取城市下拉框选项 */
export function selectCityDropdownList() {
  return request({
    url: '/v1/city/selectCityDropdownList',
    method: 'get'
  })
}
