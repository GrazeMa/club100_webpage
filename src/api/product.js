import request from '@/utils/request'

/** 查询产品列表 */
export function listProduct(query) {
  return request({
    url: '/v1/product/listProduct',
    method: 'get',
    params: query
  })
}

/** 删除产品信息 */
export function deleteProduct(returnProductInfoIds) {
  return request({
    url: '/v1/product/' + returnProductInfoIds,
    method: 'delete'
  })
}

/** 查询产品信息 */
export function getProductById(returnProductInfoId) {
  return request({
    url: '/v1/product/getProductById',
    method: 'post',
    data: returnProductInfoId
  })
}

/** 修改产品信息 */
export function updateProduct(data) {
  return request({
    url: '/v1/product/updateProduct',
    method: 'post',
    data: data
  })
}

/** 添加产品信息 */
export function addProduct(data) {
  return request({
    url: '/v1/product/addProduct',
    method: 'post',
    data
  })
}

/** 获取活动单选下拉框选项 */
export function selectActivityDropdownList() {
  return request({
    url: '/v1/activity/selectActivityDropdownList',
    method: 'get'
  })
}