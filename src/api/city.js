import request from '@/utils/request'

/** 查询城市列表 */
export function listCities(query) {
  return request({
    url: '/v1/city/list',
    method: 'get',
    params: query
  })
}

/** 查询城市信息 */
export function getCityById(cityId) {
  return request({
    url: '/v1/city/getCityById',
    method: 'post',
    data: cityId
  })
}

/** 修改城市信息 */
export function updateCity(data) {
  return request({
    url: '/v1/city/update',
    method: 'post',
    data: data
  })
}

/** 删除城市信息 */
export function deleteCities(cityIds) {
  return request({
    url: '/v1/city/' + cityIds,
    method: 'delete'
  })
}

/** 添加一条城市信息 */
export function addCity(data) {
  return request({
    url: '/v1/city/addCity',
    method: 'post',
    data
  })
}

/** 上线按钮操作 */
export function uploadCity(cityIds) {
  return request({
    url: '/v1/city/uploadCity',
    method: 'post',
    data: cityIds
  })
}

// 导出岗位
// export function exportPost(query) {
//   return request({
//     url: '/system/post/export',
//     method: 'get',
//     params: query
//   })
// }