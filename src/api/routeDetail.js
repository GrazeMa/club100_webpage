import request from '@/utils/request'

/** 查询路线内容 */
export function page(query) {
  return request({
    url: '/v1/routeDetail',
    method: 'get',
    params: query
  })
}
